package com.example.menu;

import android.app.Fragment;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment2 extends Fragment {
    String selection;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2, container, false);
        selection = ((MainActivity) getActivity()).getSelectedSocialNetwork();
        textView = (TextView) view.findViewById(R.id.textView);

        switch (selection){
            case "Vkontakte":
                textView.setText("«ВКонта́кте»[2] (международное название — VK) — российская социальная сеть со штаб-квартирой в Санкт-Петербурге. Сайт доступен на 82 языках[1]; особенно популярен среди русскоязычных пользователей. «ВКонтакте» позволяет пользователям отправлять друг другу сообщения, редактировать эти сообщения[3], создавать собственные страницы и сообщества, обмениваться изображениями, аудио- и видеозаписями, переводить деньги, играть в браузерные игры. Также позиционирует себя платформой для продвижения бизнеса и решения повседневных задач с помощью мини-приложений[4]. Наиболее популярна в России, Белоруссии, Казахстане и Узбекистане, заблокирована на Украине с мая 2017 года.");
                break;
            case "Telegram":
                textView.setText("Telegram (от др.-греч. τῆλε «далеко» + др.-греч. γράμμα «запись») — кроссплатформенная система мгновенного обмена сообщениями (мессенджер) с функциями обмена текстовыми, голосовыми и видеосообщениями, а также стикерами, фотографиями и файлами многих форматов[6]. Также позволяет совершать аудио- и видеозвонки, устраивать прямые эфиры в каналах и группах, организовывать конференции, многопользовательские группы и каналы. Функциональность приложения может быть значительно расширена при помощи ботов. Клиентские приложения Telegram доступны для Android, iOS, Windows, macOS и GNU/Linux[7].");
                break;
        }
        return view;
    }
}