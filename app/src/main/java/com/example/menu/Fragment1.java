package com.example.menu;

import android.app.Fragment;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class Fragment1 extends Fragment {

    ListView listView;
    String[] socialNetworks = {"Vkontakte", "Telegram"};
    String selection;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1, container, false);
        listView = view.findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, socialNetworks);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selection = (String) parent.getItemAtPosition(position);
                ((MainActivity)getActivity()).setSelectedSocialNetwork(selection);
//                ((MainActivity)getActivity()).loadFragment(new Fragment2());
                View v =  ((MainActivity)getActivity()).bottomNavigationView.findViewById(R.id.menuItemFrag2);
                v.performClick();

            }
        });
        return view;
    }
}