package com.example.menu;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    public String selectedSocialNetwork;

    public String getSelectedSocialNetwork() {
        return selectedSocialNetwork;
    }

    public void setSelectedSocialNetwork(String selectedSocialNetwork) {
        this.selectedSocialNetwork = selectedSocialNetwork;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navBar);
        loadFragment(new Fragment1());

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menuItemFrag1:
                        loadFragment(new Fragment1());
                        break;
                    case R.id.menuItemFrag2:
                        loadFragment(new Fragment2());
                        break;
                }
                return false;
            }
        });

    }

    public void setMenuItemChecked(MenuItem item){
        item.setChecked(true);
    }

    public void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit();
    }
}
